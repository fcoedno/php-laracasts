<?php

/**
 *
 */
class Connection
{


  public static $pdo;

  private function __construct() {
      //
  }

  public static function make($config)
  {
    try {

      if(!isset(self::$pdo)) { // verifica se já existe instância do pdo

        // Conexão e configuração do banco
        self::$pdo = new PDO(
          $config['connection'].';'.'dbname='.$config['name'],
          $config['username'],
          $config['password'],
          $config['options']
        );

      }

      return self::$pdo; // retorna a instância

    } catch (PDOException $e) {
      dd($e->getMessage());
    }
  }

}
