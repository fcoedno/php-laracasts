<?php

namespace App\Controllers;

use App\Models\Task;


class PagesController
{

  public function home()
  {
    $tasks = Task::all();

    return view('index', compact('tasks'));
  }

  public function about()
  {
    $company = 'Excellence Soft';

    return view('about', compact('company'));
  }

  public function contact()
  {
    return view('contact');
  }

}
