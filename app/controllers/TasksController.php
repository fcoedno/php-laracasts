<?php

namespace App\Controllers;

use App\Core\Request;
use App\Models\Task;

class TasksController
{
  public function store(Request $request)
  {
    $task = new Task($request->task);
    $task->save();

    redirect('');
  }
}
