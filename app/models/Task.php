<?php

namespace App\Models;

use App\Core\App;

class Task
{

  public static function all()
  {
    return App::get('database')->selectAll('todos', Task::class);
  }

  public function save()
  {
    App::get('database')->insert(
      'todos',
      ['description' => $this->description, 'completed' => $this->completed]
    );
  }

  // Construtores e destrutores
  public function __construct($description = '', $completed = false)
  {
    $this->description = $description;
    $this->completed = $completed;
  }

  // Métodos públicos

  // Getters e setters
  public function getDescription()
  {
    return $this->description;
  }

  public function setDescription($description)
  {
    $this->description = $description;
  }

  public function isComplete()
  {
    return $this->completed;
  }

  public function complete()
  {
    $this->completed = true;
  }

  // Propriedades
  protected $description;
  protected $completed = false;
}
