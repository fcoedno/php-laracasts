<?php

use App\Core\Router;
use App\Core\Request;

// Entry point: Responsible for bootstrapping and setting out

require 'app/core/bootstrap.php';

$router = new Router();

Router::load('app/routes.php')->direct(Request::uri(), Request::method());
