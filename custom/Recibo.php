<?php

// include autoloader
require_once 'dompdf/autoload.inc.php';
require 'Monetary.php';

// reference the Dompdf namespace
use Dompdf\Dompdf;

/**
 *
 */
class Recibo
{

  public static function generate($vias, $value, $company, $reference, $date, $emitter = "Edvan Farias", $crc = "CRC: 9409/0-3")
  {
    $recibo = new Recibo($value, $company, $reference, $date, $emitter, $crc);
    $recibo->viewPDF($vias);
  }

  public function __construct($value, $company, $reference, $date, $emitter = "Edvan Farias", $crc = "CRC: 9409/0-3")
  {
    $this->value = $value;
    $this->company = $company;
    $this->reference = $reference;
    $this->date = $date;
    $this->emitter = $emitter;
    $this->crc = $crc;

    $this->dompdf = new Dompdf();
    $this->dompdf->setPaper('A4', 'portrait');

    // $this->template1 = "<html><head><style>.container{border: 1px solid black; font-size: 12pt;}#segundaVia{margin-top: 50px;}.center{text-align: center;}p{margin: 0 20px 10px; clear: both;}h2{margin-top: 10px; font-size: 16pt}.valor{float: right; margin-right: 20px; margin-bottom: 0px; margin-top: 0px; font-size: 14pt; font-weight: bold;}hr{border-top: 1px solid black; width: 300pt; margin: 0px auto 5px;}.contador{margin: 0px 0px 20px;}img{width: 100%; height: auto;}.primeira-parte{margin: 10px 0px;}.primeira-parte p{margin-top: 3px; margin-bottom: 3px;}.segunda-parte{margin: 20px 0px 20px;}.data{margin-bottom: 40px;}</style></head><body> <div class='container'> <img src="images/header.jpg"> <h2 class='center'>RECIBO</h2> <p class="valor">VALOR: R${{$value}}</p><div class="primeira-parte"> <p>Recebi(emos) de:{{$company}}</p><p>A importância de:{{$value_text}}</p><p>Referente a:{{$reference}}</p></div><div class="segunda-parte"> <p>Para maior clareza firmo(amos) o presente</p></div><div class="center"> <p class="data">{{$date}}</p><hr> <p class="contador">{{$emitter}}{{$crc}}</p></div></div></body></html>';//$_SERVER['DOCUMENT_ROOT'].'/custom/recibo1.pdf.html";
    $this->template1 = '';
    $this->template2 = '<html><head><style>.container{border: 1px solid black; font-size: 12pt;}#segundaVia{margin-top: 50px;}.center{text-align: center;}p{margin: 0 20px 10px; clear: both;}h2{margin-top: 10px; font-size: 16pt}.valor{float: right; margin-right: 20px; margin-bottom: 0px; margin-top: 0px; font-size: 14pt; font-weight: bold;}hr{border-top: 1px solid black; width: 300pt; margin: 0px auto 5px;}.contador{margin: 0px 0px 20px;}img{width: 100%; height: auto;}.primeira-parte{margin: 10px 0px;}.primeira-parte p{margin-top: 3px; margin-bottom: 3px;}.segunda-parte{margin: 20px 0px 20px;}.data{margin-bottom: 40px;}</style></head><body> <div class="container"> <img src="images/header.jpg"> <h2 class="center">RECIBO</h2> <p class="valor">VALOR: R${{$value}}</p><div class="primeira-parte"> <p>Recebi(emos) de:{{$company}}</p><p>A importância de:{{$value_text}}</p><p>Referente a:{{$reference}}</p></div><div class="segunda-parte"> <p>Para maior clareza firmo(amos) o presente</p></div><div class="center"> <p class="data">{{$date}}</p><hr> <p class="contador">{{$emitter}}{{$crc}}</p></div></div><div class="container" id="segundaVia"> <img src="images/header.jpg"> <h2 class="center">RECIBO</h2> <p class="valor">VALOR: R${{$value}}</p><div class="primeira-parte"> <p>Recebi(emos) de:{{$company}}</p><p>A importância de:{{$value_text}}</p><p>Referente a:{{$reference}}</p></div><div class="segunda-parte"> <p>Para maior clareza firmo(amos) o presente</p></div><div class="center"> <p class="data">{{$date}}</p><hr> <p class="contador">{{$emitter}}{{$crc}}</p></div></div></body></html>';
    //$_SERVER['DOCUMENT_ROOT'].'/custom/recibo2.pdf.html';
  }

  public function viewPDF($vias = 1)
  {
    // carrega o html correto
    $this->loadHtml($vias);

    // Render the HTML as PDF
    $this->dompdf->render();

    // Output the generated PDF (1 = download and 0 = preview)
    $this->dompdf->stream("recibo-20032017",array("Attachment"=>0));
  }

  public function downloadPDF($vias = 1)
  {
  }

  public function getTemplate($n)
  {
    return $n == 1? $this->template1 : $this->template2;
  }


  protected function loadHtml($vias)
  {
    $this->html = $this->template2;//file_get_contents($this->getTemplate($vias));

    $this->html = str_replace('{{$company}}', mb_strtoupper($this->company), $this->html);
    $this->html = str_replace('{{$value}}', number_format($this->value, 2, ',', '.'), $this->html);
    $this->html = str_replace('{{$value_text}}', ucfirst(Monetary::numberToExt($this->value)), $this->html);
    $this->html = str_replace('{{$reference}}', mb_strtoupper($this->reference), $this->html);
    $this->html = str_replace('{{$date}}', $this->date, $this->html);
    $this->html = str_replace('{{$emitter}}', $this->emitter, $this->html);
    $this->html = str_replace('{{$crc}}', $this->crc, $this->html);

    $this->dompdf->loadHtml($this->html);
  }

  protected $value;
  protected $company;
  protected $reference;
  protected $date;
  protected $emitter;
  protected $crc;

  protected $dompdf;
  protected $html;
  protected $template1;
  protected $template2;
}
